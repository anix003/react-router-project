export const isToken = ()=> {
    const auth = !!localStorage.getItem('token');
    return auth;
}