import React from 'react'
import {Routes, Route} from 'react-router-dom'
import Dashone from './modules/Dashone'
import DashTwo from './modules/DashTwo'

export default function DashboardLayout() {
  return (
    <Routes>
        <Route path="/" element={<Dashone/>}/>
        <Route path="/two" element={<DashTwo/>}/>
    </Routes>
  )
}
