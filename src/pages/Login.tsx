import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

export default function Login() {

    const navigate = useNavigate()
   
    const [formValues, setFormValues] = useState({
        email:'',
        password:''
    })

    const inputHandler = (e:React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault()
      setFormValues({
        ...formValues,
        [e.target.name]:e.target.value
      })
    }
    const loginHandler = async(e: React.FormEvent) => {
        e.preventDefault()
      const response:any = await axios.post('http://127.0.0.1:8000/api/login-custom', formValues)
      try{
        console.log("response", response);
        response?.data?.token && (
            localStorage.setItem('token', response?.data?.token)
        )
        navigate('/')
      }
      catch(err:any){
        console.log(err);
      }
    }
  return (
    <form onSubmit={loginHandler}>
        <label htmlFor="login">Email</label>
        <input name='email' type="text" value={formValues?.email} onChange={inputHandler}/>
        <label htmlFor="password">password</label>
        <input name='password' type="password" value={formValues?.password} onChange={inputHandler}/>
        <button type='submit'>Submit</button>
    </form>
  )
}
