/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react'
import {Routes, Route, Navigate, useLocation} from 'react-router-dom'
import AuthLayout from '../modules/auth/AuthLayout'
import DashboardLayout from '../modules/dashboard/DashboardLayout'
import { isToken } from '../plugins/https'

const MainRoute = () => {


    const loggedInAuth = isToken();
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(loggedInAuth) 
    const setDefaultPath = isLoggedIn ? '/dashboard' : '/auth';

    const route = useLocation().pathname

    useEffect(()=>{
        setIsLoggedIn(loggedInAuth)
    },[route])

    console.log("auth", isLoggedIn)

    return(
        <Routes>
        <Route 
          path="/auth/*"
          element={!isLoggedIn ? <AuthLayout/> : <Navigate to='/dashboard'/>}
          />
          <Route 
          path= "/dashboard/*"
          element ={isLoggedIn ? <DashboardLayout/> : <Navigate to='/auth'/>}
          />
          <Route path="*" element={<Navigate to={setDefaultPath} />} />
        </Routes>
    )
}

export default MainRoute